0.4.5 / 2016-01-02
==================

  * Open URL with ID

0.4.4 / 2015-12-30
==================

  * Added delete single bookmark function

0.4.0 / 2012-03-30 
==================

  * Added new HTML style

0.3.0 / 2012-03-15 
==================

  * Added: use webkit2png for thumbs. Closes #2

0.2.0 / 2012-03-15 
==================

  * Added nicer display for large urls
  * Added: strip http:// when listing

0.1.0 / 2012-03-15 
==================

  * Added experimental screenshots feature

0.0.1 / 2012-03-15 
==================

  - Initial release
